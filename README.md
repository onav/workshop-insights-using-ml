# Extracting Insights from Data Using Machine Learning
In this Workshop You Will Learn
1. Brief Introduction to Machine Learning
1. Enough Python to Access Machine Learning Tools
1. Hands-on Developing of Machine Learning Models and Extracting Insights
1. Practical Tips & Common Mistakes

## Setup Instructions
For this workshop we recommend installing the <a href="https://www.anaconda.com/products/individual" target="_blank">Anaconda Python Distribution</a> which supports Windows, Mac, and Linux.  This single all-in-one installer includes:
1. Python
2. Many of the useful data analaysis packages
3. <a href="https://jupyter.org/" target="_blank">Jupyter Notebook</a>, which is the environment we will use in the workshop

All of this will be explained in the workshop.  For now, we need you to download and install Anaconda.

### 1. Anaconda Python Distribution

1. Navigate to <a href="https://www.anaconda.com/products/individual#Downloads" target="_blank">https://www.anaconda.com/products/individual#Downloads</a> with your web browser and download the Python *3.x* installer (e.g. Python 3.7).
2. Run the installer using all the default installation options.

**Note for Linux Users:** *Running the installer requires using the shell, as described [here](https://docs.anaconda.com/anaconda/install/linux/#installation).*

Send us a message at [team@iterativelearning.io](mailto:team@iterativelearning.io) if you need support with this step.

### 2. Browser Compatibility
The Jupyter Notebook runs in the browser and supports: [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Chrome](https://www.google.com/chrome/), and [Safari](https://support.apple.com/downloads/safari).  Up to date versions of Opera and Edge may also work, but if they don’t, please have one of the supported browsers installed.